import subprocess
import time
import schedule

def job():
    proc = subprocess.Popen(['python', 'main.py'])
    time.sleep(50)
    try:
        subprocess.Popen(["kill", str(proc.pid)])
    except:
        pass

schedule.every(15).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
