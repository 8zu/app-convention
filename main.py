import sys
from balsa import get_logger, Balsa
from src.bot import *
from src.util.util import get_config

app_name = "tc_convention_center_forward"

balsa = Balsa(app_name, '8zu', verbose=True)
balsa.init_logger()
logger = get_logger(app_name)

config = get_config("config.toml", logger)
config['app_name'] = app_name
if "channel_id" not in config:
    logger.error("Channel ID missing. Exiting")
    sys.exit(0)

bot = init(config)
bot.run(config['discord_bot_token'])
