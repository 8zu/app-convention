#!/usr/bin/env bash

function download_chromedriver_if_necessary() {
  if [ -f chromedriver ]; then
    echo "chromedriver is already here"
    exit 0
  else
    wget https://chromedriver.storage.googleapis.com/2.45/chromedriver_linux64.zip
    unzip chromedriver_linux64.zip
    rm chromedriver_linux64.zip
  fi
}

download_chromedriver_if_necessary
