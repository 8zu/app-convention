from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..model.message import Base, Message
from ..extraction.convention_feed import Post

def dt(ts):
    return datetime.fromtimestamp(ts)

# Initialize
engine = create_engine('sqlite:///test.db')
Base.metadata.create_all(engine)
Base.metadata.bind = engine
session = sessionmaker(bind=engine)()

# Insert some dummy data
posts = [
    Post(timestamp=dt(1), author_hash="abc", author_name="foo", color=0xFFFFFF, content="first"),
    Post(timestamp=dt(2), author_hash="abc", author_name="foo", color=0xFFFFFF, content="second"),
    Post(timestamp=dt(3), author_hash="abc", author_name="foo", color=0xFFFFFF, content="third")
]

from ..model.db import DBAccess

db = DBAccess(db_path='sqlite:///test.db')
with db.transaction():
    for p in posts:
        db.save_message_no_commit(p)

assert(session.query(Message).count() == 3)
assert(db.get_latest_timestamp().get() == dt(3))
after1 = db.get_messages_no_later_than(dt(2))
assert(len(after1) == 2)
assert(after1[0].timestamp == 2)
assert(after1[1].timestamp == 3)
print("All pass!")

# Tear down
import os
os.unlink("test.db")
