from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..model.message import Base, Message

engine = create_engine('sqlite:///data.db')
Base.metadata.create_all(engine)
Base.metadata.bind = engine
session = sessionmaker(bind=engine)()

session.add(Message(timestamp=1, author_hash="abcdefgh", author_name="foo", content="foo"))
session.add(Message(timestamp=2, author_hash="abcdefgh", author_name="foo", content="bar"))
session.commit()
