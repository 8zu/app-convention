import sys
from ..extraction.extractor import get_all_posts, get_posts_newer_than
from ..model.db import DBAccess

if len(sys.argv) < 2:
    print(f"Usage: python -m src.bin.download_all_posts <url>")
    sys.exit(0)
url = sys.argv[1]
log_every = 30

db = DBAccess('sqlite:///data.db')
latest = db.get_latest_timestamp()
if latest.is_defined():
    print("Database is not empty. Will use the latest timestamp in database to fetch evrything afterwards")
    feed = get_posts_newer_than(url, latest.get())
else:
    print("Database is empty, download everything")
    feed = get_all_posts(url)


print('Start download all posts')
try:
    with db.transaction():
        for i, post in enumerate(feed, start=1):
            db.save_message_no_commit(post)
            if i % log_every == 0:
                print(f"I have downloaded {i} posts")
except ConnectionError as ex:
    print(ex)
