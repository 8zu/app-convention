from collections import namedtuple
from datetime import datetime
import re
import time
from pyquery import PyQuery as pq
from typing import List
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from ..util.util import pick_color

Post = namedtuple('Post', ['timestamp', 'author_hash', 'author_name',
                           'color', 'content'])
def make_post(msg) -> Post:
    return Post(timestamp=datetime.fromtimestamp(msg.timestamp),
                author_hash=msg.author_hash,
                author_name=msg.author_name,
                color=pick_color(msg.author_hash),
                content=msg.content)

def create_site_driver() -> "driver":
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-gpu')
    options.add_argument('--window-size=1280,1024')
    options.add_argument('--ignore-certificate-errors')
    return webdriver.Chrome(options=options)


def get_html(driver, url) -> pq:
    driver.get(url)
    time.sleep(5)
    return pq(driver.page_source, parser='html')


def parse_date(dt: str) -> datetime:
    return datetime.strptime(dt, '%m/%d/%Y %I:%M:%S %p')


color_regex = re.compile('color: rgb\((?P<r>\d+), (?P<g>\d+), (?P<b>\d+)\);')


def parse_color(c: str) -> int:
    """
    expect input in the format of
        color: rgb(39, 73, 134);
    transform this to a integer representing the RGB value
    """
    r, g, b = map(int, color_regex.match(c).groups())
    return (r << 16) + (g << 8) + b


def parse_posts(html: pq) -> List[Post]:
    posts = html('.election-cs-post')
    return [Post(
        timestamp=parse_date(post('.election-cs-post-date').text()),
        author_hash=post('.election-cs-post-hash').text(),
        author_name=post('.election-cs-post-name').text(),
        color=parse_color(post('.election-cs-post-name').attr('style')),
        content=post('div.election-cs-post-body > p').text()
    ) for post in posts.items()]


def load_more_post(driver) -> pq:
    els = driver.find_elements_by_class_name('link-unstyled')
    if els:
        els[0].click()
    else:
        raise RuntimeError('Cannot find button.')
    time.sleep(5)
    return pq(driver.page_source, parser='html')


def skip_seen_posts(last_seen_timestamp, posts: List[Post]) -> List[Post]:
    new_posts = filter(lambda p: p.timestamp < last_seen_timestamp,
                       posts) if last_seen_timestamp else posts
    return list(new_posts)


def convention_feed(url: str) -> Post:
    driver = create_site_driver()
    last_seen_timestamp = None
    html = get_html(driver, url)
    while True:
        posts = parse_posts(html)
        new_posts = skip_seen_posts(last_seen_timestamp, posts)
        if not new_posts:
            return
        for post in new_posts:
            yield post
        last_seen_timestamp = post.timestamp
        html = load_more_post(driver)
