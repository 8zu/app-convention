from itertools import takewhile
from datetime import datetime
from .convention_feed import convention_feed


def get_all_posts(url: str):
    yield from convention_feed(url)


def get_posts_newer_than(url: str, dt: datetime):
    feed = convention_feed(url)
    yield from takewhile(lambda p: p.timestamp > dt, feed)
