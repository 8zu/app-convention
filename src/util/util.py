import colorsys
import logging
import os.path as osp
import sys
import pytoml

def get_config(path: str, logger) -> dict:
    if osp.exists(path):
        config = pytoml.load(open(path, "r", encoding="UTF-8"))
    else:
        logger.error("Missing config file! Shutting down now...")
        sys.exit(1)
    if 'discord_bot_token' not in config or not config['discord_bot_token']:
        logger.error("Token is not filled in! Shutting down now...")
        sys.exit(1)
    return config

def pick_color(s) -> int:
    h, l, s = hash(s) % 360 / 360, 0.5, 0.5
    r, g, b = map(lambda x: int(x * 255), colorsys.hls_to_rgb(h, l, s))
    return (r << 16) + (g << 8) + b
