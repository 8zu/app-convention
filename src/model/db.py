from contextlib import contextmanager
from datetime import datetime
from typing import List
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .message import Base, Message
from ..extraction.convention_feed import Post
from ..util.option import Option, Some, Non


class DBAccess(object):
    def __init__(self, db_path):
        engine = create_engine(db_path)
        Base.metadata.create_all(engine)
        Base.metadata.bind = engine
        self.session = sessionmaker(bind=engine)()

    def get_latest_timestamp(self) -> Option[datetime]:
        latest = self.session.query(Message) \
                     .order_by(Message.timestamp.desc()) \
                     .first()
        if latest:
            return Some(datetime.fromtimestamp(latest.timestamp))
        else:
            return Non

    def get_messages_no_later_than(self, thresh: datetime) -> List[Message]:
        return self.session.query(Message) \
                           .filter(Message.timestamp >= thresh.timestamp()) \
                           .order_by(Message.timestamp.asc()) \
                           .all()

    @contextmanager
    def transaction(self):
        yield
        self.session.commit()

    def save_message_no_commit(self, post: Post):
        self.session.add(Message.from_post(post))

    def save_messages(self, posts: List[Post]) -> bool:
        with self.transaction():
            for post in posts:
                self.save_message_no_commit(post)
        return True
