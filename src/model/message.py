import math
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from ..extraction.convention_feed import Post

Base = declarative_base()


class Message(Base):
    __tablename__ = "message"
    id = Column(Integer, primary_key=True)
    timestamp = Column(Integer)
    author_hash = Column(String(8))
    author_name = Column(String(128))
    content = Column(String)

    @staticmethod
    def from_post(post: Post):
        return Message(
            timestamp=math.floor(post.timestamp.timestamp()),
            author_hash=post.author_hash,
            author_name=post.author_name,
            content=post.content
        )
