from datetime import datetime
from random import random
import time
from typing import List
from balsa import get_logger
import discord
from .extraction.extractor import get_posts_newer_than, get_all_posts
from .extraction.convention_feed import Post, make_post, parse_date
from .model.db import DBAccess


def init(config: dict) -> discord.Client:
    logger = get_logger(config['app_name'])
    bot = discord.Client()
    db = DBAccess('sqlite:///data.db')
    url = config['board_url']

    def save_into_db_then_reverse(db, feed):
        count = 0
        alert_every = 30
        with db.transaction():
            for new_post in feed:
                db.save_message_no_commit(new_post)
                count += 1
                if count % alert_every == 0:
                    logger.info(f"I have fetched {count} posts")
                oldest_timestamp = new_post.timestamp
        logger.info(f"Fetched {count} new posts in total")
        if count > 0:
            for msg in db.get_messages_no_later_than(oldest_timestamp):
                yield make_post(msg)

    def render_post(post: Post) -> discord.Embed:
        return discord.Embed(type="rich",
                             title=f"{post.author_name} ({post.author_hash})",
                             timestamp=post.timestamp,
                             description=post.content,
                             color=post.color)

    async def post_message(channel, post: Post):
        """Be careful about the rate limiting bucket"""
        await bot.send_message(channel, embed=render_post(post))
        time.sleep(0.5 + random())

    @bot.event
    async def on_ready():
        channel = bot.get_channel(config['channel_id'])
        if not channel:
            logger.error("The channel is not found. Please check if the channel" +
                         "exists, if the bot can see it, or has the priviledge to view channels")
            await bot.close()
            return

        latest_timestamp = db.get_latest_timestamp()
        if latest_timestamp.is_defined():
            logger.info(
                f"Currently latest timestamp is {latest_timestamp.get()}. Will fetch posts after this.")
            feed = get_posts_newer_than(url, latest_timestamp.get())
        else:
            logger.info("Database is empty. This will load all the past posts. It might take a while.")
            feed = get_all_posts(url)

        if config['flush_everything_from_database']:
            if isinstance(config['flush_start_time'], str):
                try:
                    start_time = parse_date(config['flush_start_time'])
                except Exception:
                    start_time = datetime.fromtimestamp(0)
            elif isinstance(config['flush_start_time']), int):
                start_time=datetime.fromtimestamp(config['flush_start_time']))
            else:
                print("Cannot recognize the time format, using 0.")
                start_time=datetime.fromtimestamp(0)
            for message in db.get_messages_no_later_than(start_time):
                await post_message(channel, make_post(message))
        else:
            for new_post in save_into_db_then_reverse(db, feed):
                await post_message(channel, new_post)
        await bot.close()
    return bot
